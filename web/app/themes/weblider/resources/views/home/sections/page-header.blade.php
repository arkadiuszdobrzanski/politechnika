<section id="home" class="hero-section img-bg ">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-xl-6">
        <div class="hero-content">
          <h1 class="wow fadeInUp" data-wow-delay=".2s">WCAG 2.1 - Lorem ipsum</h1>
          <p class="wow fadeInUp" data-wow-delay=".4s">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
          </p>
          <a href="#service" rel="nofollow" class="main-btn btn-hover wow fadeInUp" data-wow-delay=".6s">Lorem ipsum</a>
        </div>
      </div>
      <div class="col-xl-6 col-lg-5">
        <div class="hero-img wow fadeInUp" data-wow-delay=".5s">
          <img src="{{\App\asset_path('images/hero-img.png')}}" />
        </div>
      </div>
    </div>
  </div>
</section>
