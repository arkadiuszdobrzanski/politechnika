<section id="pricing" class="pricing-section pt-130 container">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xxl-5 col-xl-6 col-lg-7">
        <div class="section-title text-center mb-60">
          <h1 class="mb-35 wow fadeInUp" data-wow-delay=".2s">Nasze plany cenowe</h1>
          <p class="wow fadeInUp" data-wow-delay=".4s">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore</p>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-lg-4 col-md-8 col-sm-10">
        <div class="single-pricing color-1">
          <div class="header">
            <h3 class="package-name">Standart</h3>
            <span class="price">3 000<small>zł</small> </span>
          </div>
          <ul>
            <li>AUDYT WCAG 2.1</li>
            <li>Projekt serwisu WCAG 2.1</li>
            <li>3 wersje kolorystyczne wysokiego kontrastu</li>
            <li>Moduł RODO</li>
            <li>Dostosowanie do AA</li>
            <li>Ponad 10 podstron</li>
          </ul>
          <a href="#contact" class="main-btn border-btn btn-hover">Kup teraz</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-8 col-sm-10">
        <div class="single-pricing color-2">
          <div class="header">
            <h3 class="package-name">Pakiet złoty</h3>
            <span class="price">5 000<small>zł</small> </span>
          </div>
          <ul>
            <li>AUDYT WCAG 2.1</li>
            <li>Projekt serwisu WCAG 2.1</li>
            <li>3 wersje kolorystyczne wysokiego kontrastu</li>
            <li>Moduł RODO</li>
            <li>Dostosowanie do AA</li>
            <li>Ponad 10 podstron</li>
          </ul>
          <a href="#home" class="main-btn btn-hover">Kup teraz</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-8 col-sm-10">
        <div class="single-pricing color-3">
          <div class="header">
            <h3 class="package-name">Pakiet Premium</h3>
            <span class="price">7 000<small>zł</small> </span>
          </div>
          <ul>
            <li>AUDYT WCAG 2.1</li>
            <li>Projekt serwisu WCAG 2.1</li>
            <li>3 wersje kolorystyczne wysokiego kontrastu</li>
            <li>Moduł RODO</li>
            <li>Dostosowanie do AA</li>
            <li>Ponad 10 podstron</li>
          </ul>
          <a href="#service" class="main-btn border-btn btn-hover">Kup teraz</a>
        </div>
      </div>
    </div>
  </div>
</section>
