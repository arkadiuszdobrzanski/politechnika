<section id="contact" class="contact-section pt-120 pb-120 mt-120">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-xl-5">
        <div class="section-title mb-60">
          <h1 class="mb-25 wow fadeInUp" data-wow-delay=".2s">Skontakuj się z nami w sprawie wyceny</h1>
          <p class="wow fadeInUp" data-wow-delay=".4s">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-7">
        <div class="contact-wrapper wow fadeInUp" data-wow-delay=".2s">
          <form action="#" method="POST" id="contact-form" class="contact-form">
            <div class="row">
              <div class="col-md-6">
                <div class="single-form">
                  <label for="name" class="form-label">Imię i nazwisko</label>
                  <input type="text" name="name" id="name" class="form-input" placeholder="Imię i nazwisko">
                </div>
              </div>
              <div class="col-md-6">
                <div class="single-form">
                  <label for="email" class="form-label">Adres email</label>
                  <input type="email" name="email" id="email" class="form-input" placeholder="Adres E-mail">
                </div>
              </div>
              <div class="col-md-6">
                <div class="single-form">
                  <label for="subject" class="form-label">Temat</label>
                  <input type="text" name="subject" id="subject" class="form-input" placeholder="Temat">
                </div>
              </div>
              <div class="col-md-6">
                <div class="single-form">
                  <label for="number" class="form-label">Numer telefonu</label>
                  <input type="text" name="number" id="number" class="form-input" placeholder="Numer telefonu">
                </div>
              </div>

              <div class="col-md-12">
                <div class="single-form">
                  <label for="message" class="form-label">Treść wiadomości</label>
                  <textarea name="message" id="message" class="form-input" rows="10" placeholder="Treść wiadomośći"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <div class="submit-btn text-center">
                  <button class="main-btn btn-hover" type="submit">Wyślij wiadomość</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="col-lg-5">
        <div class="contact-img wow fadeInUp" data-wow-delay=".5s">
          <img src="{{ \App\asset_path('images/contact-img.jpg') }}" alt="Zdjęcie do formularza kontaktowego">
        </div>
      </div>
    </div>
  </div>
</section>
