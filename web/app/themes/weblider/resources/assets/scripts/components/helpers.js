/**
 * @author arkadiusz.dobrzanski@weblider.eu
 */


export {getFormData, nl2br, array_filter, strToArray}

$(document).ready(function () {
  // wyskakująca pomoc:
  // <a href="javascript:void(0)" class="btn-help" data-toggle="popover" title="Tytuł" data-content="Treść" [data-placement="right"]>
  //   <i class="fas fa-question-circle"></i>
  // </a>
    $('[data-toggle="popover"]').popover();
});

//Serializacja danych
function getFormData($form)
{

    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}


/**
 * End line to <br>
 *
 * @param {String} str
 * @returns {String}
 */
function nl2br(str)
{
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
}

/**
 * Clear array
 * eg use:
 * var $pn = pn.split('/').filter(array_filter);
 *
 * @param {Array} $array
 * @returns {Array}
 */
function array_filter($array)
{
    return $array !== '';
}

/**
 * strToArray
 * eg use:
 * var $elements = strToArray('|', 'any|string');
 *
 * @param {String} divider
 * @param {String} str
 * @returns {Object}
 */
function strToArray(divider, str)
{
    return str.split(divider).filter(array_filter);
}
