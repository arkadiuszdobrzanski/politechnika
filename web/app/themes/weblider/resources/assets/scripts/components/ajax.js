export {submitData}
// import {showMessage} from './message';
import {getFormData} from './helpers';



function doGetSubmit(method, searchValue, url, fill)
{
    let request = $.ajax({
       //uderzam do customowego routingu
        url: url + '/' + searchValue,
        type: method,
        fail: function () {},
        success: function (data) {
          //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
            console.log(searchValue);
            $(fill).hide().fadeOut(250).html(data).fadeIn(350);
          //Więc wrzucam 'data' do tej klasy
        },
    });
    console.log(request);
}
function doPostSubmit(method, searchValue, url, fill, formId)
{
    var $form = $(formId);
    // var $inputs = $form.find('input, select, button, textarea');
    // $inputs.prop('disabled', true);
    var serializedData = getFormData($form);
    console.log(serializedData);
    let request = $.ajax({
      //uderzam do customowego routingu
        url: url + '/',
        type: method,
        data: serializedData,
        fail: function (jqXHR, textStatus, errorThrown) {
            console.error(
                'Problem, error: ' +
                textStatus,
                errorThrown
            );
        },
        success: function (data) {
          //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
            console.log(searchValue);
            $(fill).hide().fadeOut(250).html(data).fadeIn(350);
          //Więc wrzucam 'data' do tej klasy
        },
    });
    console.log(request);

}


function submitData(method, searchValue, url, fill, formId)
{
    if (method == 'POST') {
        doPostSubmit(method, searchValue, url, fill, formId)
    } else if (method == 'GET') {
        doGetSubmit(method, searchValue, url, fill)
    } else {
        console.log('Niepoprawna metoda');
        return false;
    }

}

