// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/_bootstrap';



import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
    common,
  // Home page
    home,
  // About Us page, note the change from about-us to aboutUs.
    aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

//https://github.com/haltu/muuri
//https://github.com/jonom/jquery-focuspoint
//http://calebjacob.github.io/tooltipster/
//https://github.com/davatron5000/FitText.js
//https://fullcalendar.io/docs/initialize-globals

// $('.main-carousel').flickity({
//   // options
//   cellAlign: 'left',
//   contain: true
// });




// https://lokeshdhakar.com/projects/lightbox2/#getting-started

// import {submitData} from './components/ajax';
// import {readCookie, createCookie, deleteCookie, checkCookie} from './components/cookie';
// // import {nl2br, array_filter, strToArray} from './components/helpers';
// // import {showMessage, hideMessage} from './components/message';
// // import {getUrlParam, getServerUrl, getSiteUrl, getPathnameElements} from './components/url';
//
// console.log('hello World');
//
// $('#foo').keyup(function () {
//     console.log('test');
//     let searchValue = $('#foo').val();
//     let method = $('#foo').data('method');
//     let fill = $('#foo').data('fill');
//     let url = $('#foo').data('url');
//     submitData(method, searchValue, url, fill,'#siemson');
// });
