export default {
    init() {
      // JavaScript to be fired on all pages
    },
    finalize() {
        let navbarToggler = document.querySelector('.navbar-toggler');
        let navbarCollapse = document.querySelector('.navbar-collapse');

        document.querySelectorAll('.page-scroll').forEach(
            e =>
            e.addEventListener('click', () => {
                navbarToggler.classList.remove('active');
                navbarCollapse.classList.remove('show')
            })
        );
    navbarToggler.addEventListener('click', function () {
        navbarToggler.classList.toggle('active');
    })
    },
};
