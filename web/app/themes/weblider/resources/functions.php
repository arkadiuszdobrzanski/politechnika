<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */


use Roots\Sage\Config;
use Roots\Sage\Container;

add_theme_support('menus');


function before_acf_options_page()
{
    ob_start();
}

function after_acf_options_page()
{

    ob_start();
    include('views/services/OptionsService/main-template.php');
    $content = ob_get_contents();
    ob_end_clean();
    echo $content;

}
/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);


// LOGIN PAGE
function my_custom_login_logo()
{
    echo '<style type="text/css">
   body {
    background-image: linear-gradient(to top, #537895 0%, #09203f 100%)
   }
   .login h1 a {
    height: 57px!important;
    width: 213px!important;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: 100% auto;
     background-image: url( https://weblider.eu/wp-content/uploads/2019/07/logo@2.png )!important;
     margin-bottom: 0px;
  }
  .login h1:after {
    content: "Strony internetowe, identyfikacja wizualna firmy, kampanie marketingowe Google AdWords.";
    display:block;
    color: #FFF;
    font-size: 13px;
  }
  .login h1 {
    border-bottom: 1px solid #bdbdbd;
    padding-bottom: 20px;
  }
  .login form {
    background-color: rgba( 255,255,255,0.10 );
  }
  .login form .input, .login form input[type=checkbox], .login input[type=text] {
    background-color: rgba( 0,0,0,0 );
    color: #FFF;
    border: 1px solid ( 255,255,255,0.4 );
  }
  .login label{
    color: #FFF;
  }
  .login #backtoblog a, .login #nav a  {
    color: #FFF;
  }
  #login form p {
    text-align: center;
  }
  .login #login_error, .login .message, .login .success {
    color: #FFF;
    background-color: rgba( 255,255,255,0.14 );
  }
  a {
    color: #FFF;
    text-decoration: underline!important;
    }

     </style>';
}

add_action('login_head', 'my_custom_login_logo');
add_filter('login_headerurl', 'custom_loginlogo_url');

function custom_loginlogo_url($url)
{

    return 'http://www.weblider.eu';
}
