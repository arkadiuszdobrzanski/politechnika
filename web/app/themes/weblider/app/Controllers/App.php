<?php

namespace App\Controllers;

use App\Services\OptionsAdminService;
use App\Services\RoutingService;
use Sober\Controller\Controller;

class App extends Controller
{

    protected $acf = true;

    private $custom_endpoints;


    public function __construct()
    {
        $this->custom_endpoints = new RoutingService();
        new OptionsAdminService();
    }

    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function getMenu()
    {
        return wp_get_nav_menu_items("Menu");
    }
}
