<?php

namespace App\Services;

use WP_Query;

class DefaultService
{
    public function tempDir(): string
    {
        return get_template_directory_uri();
    }

    public function imageDir($string)
    {
        return $this->tempDir() . '/assets/images/' . $string;
    }

    public function cssDir($string)
    {
        return $this->tempDir() . '/assets/css/' . $string;
    }
    public function jsDir($string)
    {
        return $this->tempDir() . '/assets/js/' . $string;
    }

    public function fileDir()
    {
        return get_template_directory();
    }
}
