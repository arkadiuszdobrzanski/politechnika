<?php
/**
 * @author arkadiusz.dobrzanski@weblider.eu
 */

namespace App\Services;

use WP_REST_Controller;

class RoutingService extends WP_REST_Controller
{

    protected $namespace = 'jobs/v1';

    public function __construct()
    {
        $this->registerRoutes();
    }

    /**
     * @param $template
     * @param array $data
     * @return false|string
     */

    public function render($template, Array $data)
    {
        extract($data);
        ob_start();
        include($template);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * Rejestrujesz nowy routing metodą kopiuj/wklej
     * callback - funkcja która ma się wykonać
     */
    public function registerRoutes()
    {
        add_action( 'rest_api_init', function () {
            register_rest_route($this->namespace, '/searchBy/', array(
                'methods' => 'POST',
                'callback' => array($this, 'searchPostBy'),
                'permission_callback' => '__return_true'
            ));

            register_rest_route($this->namespace, '/searchBy/(?P<slug>[^.]+)', array(
                'methods' => 'GET',
                'callback' => 'searchPostBy',
                'permission_callback' => '__return_true'
            ));
        });
    }

    //przykładowa funkcja
    public function searchPostBy($data)
    {

    }
}
