<?php

namespace App\Services;

use WP_Query;

class WordpressPostsService
{
    public $args;

    public function __construct()
    {
    }
    /**
     * Domyślnie szuka wszystkich postów
     *
     * @param null $offset <- miejsce od któregon zaczyna się zliczanie (np. chcesz pobrać wszystkie posty pomijając dwa pierwsze to wpisujesz $offset = 2)
     * @param null $searchSlug <- Dla wyszukiwarki
     * @param int $posts_per_page <- ile potrzebujesz postów na strone (-1 oznacza że pobierze wszystkie)
     * @param null $category_name <- jeśli potrzebujsz postów tylko z danej kategorii, wpisz jej nazwę (nie slug)
     * @param string $post_type <- jeśli potrzebujesz custom post types
     * @param string $order <- sortowanie, ASC lub DESC
     * @param string $page <- w przypadku kiedy robisz paginację, wysyłasz tutaj nr strony a $offset ustawiasz null)
     * @return int[]|\WP_Post[]|WP_Query
     */
    public function getPosts($offset = null, $searchSlug = null, $posts_per_page = -1, $category_name = null, $post_type = 'post', $order = 'DESC', $page = null)
    {
        $this->args = array(
            'post_type'=> $post_type,
            'category_name' => $category_name,
            'orderby'    => 'date',
            'post_status' => 'publish',
            'orderBy' => 'id',
            'order'    => $order,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'page' => $page,
        );

        if ($searchSlug) {
            $this->args['s'] = $searchSlug;
        }

        $result = new WP_Query($this->args);
        return $result->posts;
    }

    public function firstPost($offset = null, $searchSlug = null, $posts_per_page = -1, $category_name = null, $post_type = 'post', $order = 'DESC', $page = null)
    {
        return $this->getPosts($offset, $searchSlug, $posts_per_page, $category_name, $post_type, $order, $page)[0];
    }
    public function lastPost($offset = null, $searchSlug = null, $posts_per_page = -1, $category_name = null, $post_type = 'post', $order = 'DESC', $page = null)
    {
        $array = $this->getPosts($offset, $searchSlug, $posts_per_page, $category_name, $post_type, $order, $page);
        return end($array);
    }
    public function countPost($offset = null, $searchSlug = null, $posts_per_page = -1, $category_name = null, $post_type = 'post', $order = 'DESC', $page = null)
    {
        return $this->getPosts($offset, $searchSlug, $posts_per_page, $category_name, $post_type, $order, $page)->post_count;
    }
}
