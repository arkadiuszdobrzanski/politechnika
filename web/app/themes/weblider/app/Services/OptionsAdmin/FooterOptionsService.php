<?php

namespace App\Services\OptionsAdmin;

class FooterOptionsService
{
    public function __construct()
    {
        acf_add_options_sub_page(array(
            'page_title'    => 'Ustawienia stopki',
            'menu_title'    => 'Stopka',
            'parent_slug'   => 'weblider-theme-general-settings',
        ));
    }
}
